package com.rhapsody.app.onemillionoutreaches.data;

import android.app.Application;

public class ApiUtils{
    // This API End point had no specification for a user with pass word - only email is needed to get the details
    // Takes only the email appended to it
    public static final String API_LOGIN_URL = "https://1millionoutreaches.rhapsodyofrealities.org/api/check_data.php?email=";

    // takes an append of the data below
    //"password=password&email=email@mail.com&fname=First_Name&lname=Last_Name&cname=Church_Name&title=e.g_Mr_or_Mrs&id=Church_id
    public static final  String API_URL_CREATE_ACCOUNT = "https://1millionoutreaches.rhapsodyofrealities.org/api/register.php?";

    // Prayer Request API
    final private String API_URL_PRAYER_REQUEST = "";// Not available yet

    // Praise reports and testimonies API
    final private String API_URL_PRAISE_TESTIMONY= "";// Not available yet

    // Praise reports and testimonies API
    final private String API_URL_NEW_FEED= "";// Not available yet

    // More APISs
    //final private String API

}
