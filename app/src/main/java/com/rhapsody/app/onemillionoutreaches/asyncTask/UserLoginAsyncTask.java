package com.rhapsody.app.onemillionoutreaches.asyncTask;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

// Params, Progress, Result - AsyncTask Takes Generics
public class UserLoginAsyncTask extends AsyncTask<String, Void, String> {
//public class UserLoginAsyncTask extends AsyncTask<Void, Void, String> {
    // creating call backs and Interface so as to get values from the result method and manipulate them.
    public DownloadTaskListener mListener;

    public interface DownloadTaskListener
    {
        public void onDownloadFinish(String result);
        public void onDownloadProgress(float progress);
    }

    private Exception exception;

    protected void onPreExecute(){
        //progressBar.setVisibility(View.VISIBLE);
        //responseView.setText("");
    }
    protected String doInBackground(String... urlArray){
        String urlString = urlArray[0];// will aways be the first element in the array
        String resultResponse;
        //String email = emailText.getText().toString(); // do all this outside the async task. takes less time
        // Do some validation here
        try{
            //URL url = new URL(API_URL + "email=" + email + "&apiKey=" + API_KEY);
            URL url = new URL(urlString);// Get the string and convert it into a URL
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try{
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while((line = bufferedReader.readLine()) != null){
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();
                // Response from the API
                resultResponse = stringBuilder.toString();

                return resultResponse;// This is returned and picked by the call back OnPostExecute

            }
            finally {
                urlConnection.disconnect();
            }

        }
        catch(Exception e){
            Log.e("ERROR", e.getMessage(), e);
            return null;

        }

    }

    protected void onPostExecute(String response){
        // the call back method will be called using resultResponse from doInBackground's returned value
        if(response == null){// Only if the error is in this class or connection related
            // Errors in the API can be traced from the response it'self
            response = "THERE WAS AN ERROR";
        }
        //progressBar.setVisibility(View.GONE);
        Log.i("INFO", response);
        //responseView.setText(response);
        // Test the response here to see that the correct API is returned using the Result class.
        // Handle the response here
        // may change the rsults into usable values here or from somewhere else by calling other methods
        /** Change the JSON String returned into a java object**/
        //Carry this to an appropriate place if possible like the OnPostExecute method to use the result.s
        try{
            JSONObject jsonObject = (JSONObject) new JSONTokener(response).nextValue();// get actual desired values from the response String
            String requestId = jsonObject.getString("requestId");
            int likelihood = jsonObject.getInt("requestId");
            JSONArray photos = jsonObject.getJSONArray("photos");

        }
        catch(JSONException JSONExcept){
            // Appropriate error handling code

        }

    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
}
