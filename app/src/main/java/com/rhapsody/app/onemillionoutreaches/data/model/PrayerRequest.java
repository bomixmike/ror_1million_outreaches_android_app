package com.rhapsody.app.onemillionoutreaches.data.model;

/**
 * Data class that captures user information for logged in users retrieved from PrayerRequestRepository
 */

public class PrayerRequest {

    private String userId;
    private String displayName;
    private String prayerRequestText;
    private String prayerRequestDate; // date the prayer request is sent

    public PrayerRequest(String userId, String displayName, String prayerRequestText, String prayerRequestDate) {
        this.userId = userId;
        this.displayName = displayName;
        this.prayerRequestText = prayerRequestText;
        this.prayerRequestDate = prayerRequestDate;
    }

    public String getUserId() {
        return userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getPrayerRequestText() {
        return prayerRequestText;
    }

    public String getPrayerRequestDate() {
        return prayerRequestDate;
    }

}
