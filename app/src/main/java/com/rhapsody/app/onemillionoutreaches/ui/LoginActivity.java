package com.rhapsody.app.onemillionoutreaches.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rhapsody.app.onemillionoutreaches.R;
import com.rhapsody.app.onemillionoutreaches.data.ApiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class LoginActivity extends AppCompatActivity {
    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SharedPreferences sharedPreferences = getSharedPreferences("UserPref", MODE_PRIVATE);
        String unique_email = sharedPreferences.getString("email","0");
        if(!unique_email.contentEquals("0")){
            startActivity(new Intent(LoginActivity.this, OneMillionActivity.class));
            Toast.makeText(getApplicationContext(),unique_email,Toast.LENGTH_LONG).show();
            finish();
        }

        final EditText usernameEditText = findViewById(R.id.username);
        EditText passwordEditText = findViewById(R.id.password);
        Button loginButton = findViewById(R.id.login);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);
        ImageButton ib_signup = findViewById(R.id.ib_sign_up);
        TextView tv_signup = findViewById(R.id.tv_sign_up);

        ib_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSignup();
            }
        });

        tv_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSignup();
            }
        });
        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                // use text switcher for the changes monitoring in text
                /**loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());**/
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // option for user using the enter key instead of using the touch
                    /**loginViewModel.login(usernameEditText.getText().toString(),
                            passwordEditText.getText().toString());**/
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*loadingProgressBar.setVisibility(View.VISIBLE);
                // perform the login process
                *//**loginViewModel.login(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
                 **/
                String username = usernameEditText.getText().toString();
                startLogin(username);
            }
        });
    }

    public void startLogin(final String email){
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        String url = ApiUtils.API_LOGIN_URL+email;
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("JSN1", response);
                try {
                    JSONObject account = new JSONObject(response);
                    String state = account.getString("state");
                    String message = account.getString("message");

                    if(state.contentEquals("true")){
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();

                        SharedPreferences sharedPreferences = getSharedPreferences("UserPref", MODE_PRIVATE);
                        SharedPreferences.Editor myEdit = sharedPreferences.edit();
                        myEdit.putString("email",email);
                        myEdit.commit();
                        startActivity(new Intent(LoginActivity.this, OneMillionActivity.class));
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                    }
                    Log.e("JSN3",state+ message);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Anything you want
                Log.e("JSN2", error.toString());
            }
        });
        requestQueue.add(stringRequest);
    }
    public void startSignup(){
        startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
        finish();
    }
    private void updateUiWithUser(View homeView) {
        // Do actions to display the first screen
        // also save the status of the user as logged in
        String welcome = getString(R.string.welcome);
        // TODO : initiate successful logged in experience
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
        // Set the Home screen or any appropriate screen
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    /**
     *  Add Async task to this activity
     *  Can be later removed and a class created for it
     * **/

    // Params, Progress, Result - AsyncTask Takes Generics
    public class LoginAsyncTask extends AsyncTask<String, Void, String> {

        private Exception exception;

        protected void onPreExecute(){
            //progressBar.setVisibility(View.VISIBLE);
            //responseView.setText("");
        }
        protected String doInBackground(String... urlArray){
            String urlString = urlArray[0];// will aways be the first element in the array
            String resultResponse;
            //String email = emailText.getText().toString(); // do all this outside the async task. takes less time
            // Do some validation here
            try{
                //URL url = new URL(API_URL + "email=" + email + "&apiKey=" + API_KEY);
                URL url = new URL(urlString);// Get the string and convert it into a URL
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try{
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while((line = bufferedReader.readLine()) != null){
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    // Response from the API
                    resultResponse = stringBuilder.toString();

                    return resultResponse;// This is returned and picked by the call back OnPostExecute

                }
                finally {
                    urlConnection.disconnect();
                }

            }
            catch(Exception e){
                Log.e("ERROR", e.getMessage(), e);
                return null;

            }

        }

        protected void onPostExecute(String response){
            // the call back method will be called using resultResponse from doInBackground's returned value
            if(response == null){// Only if the error is in this class or connection related
                // Errors in the API can be traced from the response it'self
                response = "THERE WAS AN ERROR";
            }
            //progressBar.setVisibility(View.GONE);
            Log.i("INFO", response);
            //responseView.setText(response);
            // Test the response here to see that the correct API is returned using the Result class.
            // Handle the response here
            // may change the rsults into usable values here or from somewhere else by calling other methods
            /** Change the JSON String returned into a java object**/
            //Carry this to an appropriate place if possible like the OnPostExecute method to use the result.s
            try{
                JSONObject jsonObject = (JSONObject) new JSONTokener(response).nextValue();// get actual desired values from the response String
                String requestId = jsonObject.getString("requestId");
                int likelihood = jsonObject.getInt("requestId");
                JSONArray photos = jsonObject.getJSONArray("photos");

            }
            catch(JSONException JSONExcept){
                // Appropriate error handling code

            }

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);

    }
}
