package com.rhapsody.app.onemillionoutreaches.data.model;

/**
 * Data class that captures user information for logged in users retrieved from UserTestimonyRepository
 * Models the data for a testimony
 */

public class Testimony {

    private String userId;
    private String displayName;
    private String testimonyText;
    private String testimonyDate; // date testimony is sent

    public Testimony(String userId, String displayName, String testimonyText, String testimonyDate) {
        this.userId = userId;
        this.displayName = displayName;
        this.testimonyText = testimonyText;
        this.testimonyDate = testimonyDate;
    }

    public String getUserId() {
        return userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getTestimonyText() {
        return testimonyText;
    }

    public String getTestimonyDate() {
        return testimonyDate;
    }
}
