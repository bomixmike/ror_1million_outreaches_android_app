package com.rhapsody.app.onemillionoutreaches.ui.ui.meter;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rhapsody.app.onemillionoutreaches.R;

public class MeterFragment extends Fragment {

    private MeterViewModel mViewModel;

    public static MeterFragment newInstance() {
        return new MeterFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.meter_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MeterViewModel.class);
        // TODO: Use the ViewModel
    }

}
