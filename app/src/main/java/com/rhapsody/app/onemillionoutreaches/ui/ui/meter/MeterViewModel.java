package com.rhapsody.app.onemillionoutreaches.ui.ui.meter;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MeterViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    private MutableLiveData<String> mText;

    public MeterViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Rhapsody Meter fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
