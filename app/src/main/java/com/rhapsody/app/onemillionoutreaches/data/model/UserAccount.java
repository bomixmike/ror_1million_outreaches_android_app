package com.rhapsody.app.onemillionoutreaches.data.model;

public class UserAccount {

    private String password;
    private String userEmail;
    private String firstName;
    private String lastName;
    private String churchName;
    private String title;
    private String churchId;

    public UserAccount(String password, String userEmail, String firstName, String lastName,
                    String churchName, String title, String churchId) {
        this.password = password;
        this.userEmail = userEmail;
        this.firstName = firstName;
        this.lastName = lastName;
        this.churchName = churchName;
        this.title = title;
        this.churchId = churchId;
    }

    public String getPassword() {
        return password;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getChurchName() {
        return churchName;
    }

    public String getTitle() { return title; }

    public String getChurchId() {
        return churchId;
    }
}
