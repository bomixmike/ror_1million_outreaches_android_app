package com.rhapsody.app.onemillionoutreaches.data;

import com.rhapsody.app.onemillionoutreaches.asyncTask.UserLoginAsyncTask;
import com.rhapsody.app.onemillionoutreaches.data.model.LoggedInUser;

import java.io.IOException;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    UserLoginAsyncTask loginUserAsync = new UserLoginAsyncTask();

    public Result<LoggedInUser> login(String username, String password) {

        try {
            // TODO: handle loggedInUser authentication
            ApiUtils loginApi = new ApiUtils();
            //email is the user name. However, password not included in the API Initially
            String loginApiEndPoint = loginApi.API_LOGIN_URL + username;
            // Run the Login async task to get the results
            loginUserAsync.execute(loginApiEndPoint);

            // Initialising the Async task. This will run whenever the button is clicked on pressed as
            // this same method is in the stack of methods called.

            // User that is logged in to the application

            LoggedInUser loggedInUser =
                    new LoggedInUser(
                            username,
                            password);

            return new Result.Success<>(loggedInUser);
            /**LoggedInUser fakeUser =
                    new LoggedInUser(
                            java.util.UUID.randomUUID().toString(),
                            "Jane Doe");
            return new Result.Success<>(fakeUser);**/
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}
