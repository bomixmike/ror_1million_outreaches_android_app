package com.rhapsody.app.onemillionoutreaches.data.model;

/**
 * Data class that captures user information for logged in users retrieved from TestimonyRepository
 */

public class NewsFeed {

    private String userId;
    private String displayName;
    // more information about the testimony
    // Other Attributes of the news feed
    private String newsArticleText;
    private String newsArticleDate;
    private String newsArticleAuthor;
    // Plus more based on the API Provided.


    public NewsFeed(String userId, String displayName, String newsArticleText, String newsArticleDate,
                    String newsArticleAuthor) {
        this.userId = userId;
        this.displayName = displayName;
        this.newsArticleText = newsArticleText;
        this.newsArticleDate = newsArticleDate;
        this.newsArticleAuthor = newsArticleAuthor;
    }

    public String getUserId() {
        return userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getNewsArticleText() {
        return newsArticleText;
    }

    public String getNewsArticleDate() {
        return newsArticleDate;
    }

    public String getNewsArticleAuthor() {
        return newsArticleAuthor;
    }


}
